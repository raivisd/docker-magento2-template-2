## Local configuration, used for developer setup with docker-compose
## Note: this file already referenced in docker-compose.yml, alter this file to override options

# Use for forcing container to run even errors occurs during the run
DOCKER_DEBUG=false

# PHP-FPM Service versions (ARG)
PHP_VERSION=7.2

# Service versions or images to use (ENV)
MYSQL_VERSION=5.7
NGINX_VERSION=alpine
REDIS_VERSION=alpine
VARNISH_VERSION=5.0
ELASTICSEARCH_VERSION=5.5.3
RABBITMQ_VERSION=alpine

# Service configurations and port mappings (ENV)
VARNISH_PORT=3000
# Varnish memory limit, defaults to 256m
#VARNISH_MEMORY=512m
NGINX_BIND_PORT=3001
MYSQL_PORT=3307
REDIS_PORT=6379
ELASTICSEARCH_PORT=9200
RABBITMQ_PORT=15672
MAILDEV_PORT=1080

### General settings (ARG)
# These settings used in build and deploy stages, set them once, recheck and deploy.
# Path where all project files are located inside container
BASEPATH=/var/www/public
# The tag to use for built and deployed image, used in app, image:
PROJECT_TAG=local
# Available deployment modes are: developer, production, default, pipeline
MAGENTO_DEPLOY_MODE=developer

# PHP Composer uses plain version number, fail if not existing
# version list, manual download section: https://getcomposer.org/download/
# default COMPOSER version: latest
# Set version for override, or leave the "latest", updated on each build
COMPOSER_VERSION=latest
COMPOSER_ALLOW_SUPERUSER=1

# Node.js is installed via n
# node versions: `n list` or https://nodejs.org/dist/
# default NODEJS version: latest LTS
NODEJS_DIR=/var/lib/node
NODEJS_VERSION=lts
NPM_CONFIG_LOGLEVEL=warn

# Ruby is installed via rbenv
# rbenv recognizes RBENV_ROOT as is. Value is also used for extending PATH definition
D_RBENV_ROOT=/var/lib/ruby
D_RBENV_VERSION=2.6.3
