#!/usr/bin/env bash
set -Eeuo pipefail

# Start the deployment timer
STARTTIME=`date +%s`

### Debug options start
# Uncomment next line for script debug
#set -euxo pipefail

# Library import
# shellcheck source=lib/*.sh
source /init/lib/color_logger.sh
source /init/lib/elapsed_time.sh

trap 'printf "\n\033[1;31mDeployment failed, check the logs above.\n\nWe cannot learn without pain\033[0m\n\n"; exit 1;' EXIT SIGINT SIGTERM

# Check project requirements
if [ -z ${COMPOSER_AUTH+x} ];
  then error "Please set COMPOSER_AUTH environment variable";
  exit 1;
fi

# Check php configuration
php-fpm -t

# In dev mode, shell exit codes are ignored, allowing for container to start
if [ $DOCKER_DEBUG = "true" ]; then
  set +e
fi

export MYSQL_PWD=$MYSQL_PASSWORD

### Debug options end

### Custom commands for assets compilation
# Add you custom tasks to execute them before magento static content deploy

################################################################################

# This script prepares docker environment for application

################################################################################

### Default env variables
export COMPOSER_NO_DEV="--no-dev"

# Execute everything in basedir
function in_basepath {
  if [ -d $BASEPATH ]; then
    # Change to base directory
    cd $BASEPATH
  else
    error "Working directory does not exist or not accessible"
    exit 1
  fi
}

# Empty the redis config caches to avoid errors with module configuration
function magento_flush_config() {
  debug "Flushing Magento config cache"
  /init/wait-for-it redis:6379 -s -t 5 -- redis-cli -h redis -n 0 flushdb
}

# This injector adds custom injects to the various points of deployment
# use filename as argument to function call
inject_override () {
  file=$1
  if [[ -f /init/custom/$file ]]; then
    debug "Applying shared override for $file"
    # shellcheck source=deploy/shared/custom/
    source /init/custom/$file
  else
    warn "Override $file does not exist or unable to load"
  fi
}

function composer_install {
  # Check if COMPOSER_AUTH environment variable exists
  if [ -z ${COMPOSER_AUTH+x} ]; then error "Please set COMPOSER_AUTH environment variable" && exit 1; fi
  # Check environment to install Dev Dependencies on specific ones
  if [ $PROJECT_TAG = "local" ]; then
    COMPOSER_NO_DEV=""
    debug "Installing composer dependencies";
    composer config vendor-dir ./vendor;
    composer install $COMPOSER_NO_DEV --ansi --no-interaction --verbose --prefer-dist;
  fi
}

function magento_config {
  debug "Setting magento database credentials"
  bin/magento setup:config:set \
    --db-host $MYSQL_HOST \
    --db-name $MYSQL_DATABASE \
    --db-user $MYSQL_USER \
    --db-password $MYSQL_PASSWORD \
    --backend-frontname $MAGENTO_ADMINURI \
    --cache-backend=redis \
    --cache-backend-redis-server=redis \
    --cache-backend-redis-db=0 \
    --session-save=redis \
    --session-save-redis-host=redis \
    --session-save-redis-log-level=3 \
    --session-save-redis-db=1 \
    --http-cache-hosts=varnish
}

function magento_install() {
  bin/magento setup:install \
    --admin-firstname $MAGENTO_FIRST_NAME \
    --admin-lastname $MAGENTO_LAST_NAME \
    --admin-email $MAGENTO_EMAIL \
    --admin-user $MAGENTO_USER \
    --admin-password $MAGENTO_PASSWORD
  if [[ $INSTALL_SAMPLE_DATA = "true" ]]; then
    # Install sample data from ScandiPWA enabled project
    if composer show | grep --quiet scandipwa/installer; then
      info "ScandiPWA detected, installing..."
      bin/magento scandipwa:theme:bootstrap Scandiweb/pwa -n
      info "Install sample data is set to true, installing for ScandiPWA..."
      debug "Importing database dump"
      wget -O- --show-progress --progress=dot:binary https://raw.githubusercontent.com/scandipwa/scandipwa-base/master/deploy/latest.sql | mysql -u $MYSQL_USER -h mysql magento
      debug "Fetching latest sample ScandiPWA media and adding it to pub/media"
      wget -O- --show-progress --progress=dot:giga https://s3-eu-west-1.amazonaws.com/scandipwa-public-assets/scandipwa_media.tgz | tar xz -C $BASEPATH/pub/media
      magento setup:upgrade
      success "Bootstrap finished, sample data is installed"

    else
      info "Install sample data is set to true, installing for Magento..."
      # Install Magento Sample data, not for PWA
      debug "Installing Magento SampleData"
      magento sampledata:deploy;
      magento setup:upgrade;
    fi
  else
    error "Sample Data install failed"
    exit 1
  fi
}

function magento_database_migration {
  # Check if magento already installed or not, ignoring exit statuses of eval, since it's magento subprocess
  set +e
  debug "Checking status of the magento database"
  local MAGENTO_STATUS
  MAGENTO_STATUS=$(magento setup:db:status)
  echo $MAGENTO_STATUS;

  # Set default value
  export ME=0;

  # We cannot rely on Magento Code, as these are update codes, not install codes. Therefore check the output for
  # the specific message!
  if [[ $MAGENTO_STATUS =~ "Magento application is not installed."$ ]]; then
    # Install sample data if needed
    magento_install
  else
    magento setup:db:status
    export ME=$?
    info "Magento db status respond code: $ME"
  fi

  ## Parse exit codes (https://github.com/magento/magento2/blob/2.2-develop/setup/src/Magento/Setup/Console/Command/DbStatusCommand.php)
  # Magento is not installed, then install it
  if [ $ME = 1 ]; then
    error "Cannot upgrade: manual action is required!"
    # Magento needs upgrade
  elif [ $ME = 2 ]; then
    info "Upgrading magento"
    if [[ $MAGENTO_DEPLOY_MODE = "production" ]] || [[ $MAGENTO_DEPLOY_MODE = "pipeline" ]]; then
      php bin/magento setup:upgrade --keep-generated;
    else
      php bin/magento setup:upgrade
    fi
    # Check returns All modules updated, move on.
  elif [ $ME = 0 ]; then
    debug "No upgrade/install is needed"
  else
    error "Database migration failed: manual action is required!"
    exit 1
  fi
  if [ $DOCKER_DEBUG != "true" ]; then
    set -e
  fi
}

function create_admin_user {
  debug "Checking user $MAGENTO_USER "
  USER_STATUS=$(bin/magento admin:user:unlock $MAGENTO_USER)
  export USER_STATUS
  if [[ $USER_STATUS =~ "Couldn't find the user account" ]]; then
    bin/magento admin:user:create \
      --admin-firstname $MAGENTO_FIRST_NAME \
      --admin-lastname $MAGENTO_LAST_NAME \
      --admin-email $MAGENTO_EMAIL \
      --admin-user $MAGENTO_USER \
      --admin-password $MAGENTO_PASSWORD
    debug "User $MAGENTO_USER created"
  fi
}

function magento_set_mode {
  # Set Magento mode
  if [ -z ${MAGENTO_DEPLOY_MODE+x} ];
    then error "Please set MAGENTO_DEPLOY_MODE environment variable to either of options: developer, default, production or pipeline";
    exit 1;
  else
    debug "Setting magento mode"
    # Clearing the generated DI if using developer mode
    if [[ $MAGENTO_DEPLOY_MODE = "developer" ]]; then rm -rf /generated/metadata/* /generated/code/*; fi;
    # Setting Magento deployment mode
    bin/magento deploy:mode:set $MAGENTO_DEPLOY_MODE --skip-compilation
  fi
}

function magento_pipeline_import_config {
  if [[ $MAGENTO_DEPLOY_MODE = "pipeline" ]]; then
    debug "Importing Magento configuration into database"
    php bin/magento app:config:import -n
  fi
}

function magento_varnish_config {
  debug "Setting varnish config for Magento, used in vcl generation"
  bin/magento config:set system/full_page_cache/varnish/access_list "127.0.0.1, 172.0.0.0, app"
  bin/magento config:set system/full_page_cache/varnish/backend_host nginx
  bin/magento config:set system/full_page_cache/varnish/backend_port 80
  bin/magento config:set system/full_page_cache/caching_application 2
}

function magento_compile {
  if [[ $MAGENTO_DEPLOY_MODE = "default" ]]; then
    debug "Generating DI"
    bin/magento setup:di:compile
  elif [[ $MAGENTO_DEPLOY_MODE = "production" ]]; then
    debug "Generating DI and assets"
    bin/magento setup:di:compile
    bin/magento setup:static-content:deploy -j 0
  fi
}

function magento_set_baseurl {
  if [[ -n ${MAGENTO_BASEURL+x} ]]; then
    debug "Setting baseurl to $MAGENTO_BASEURL"
    php bin/magento setup:store-config:set --base-url="$MAGENTO_BASEURL"
    if [[ -z ${MAGENTO_SECURE_BASEURL+x} ]]; then
      php bin/magento setup:store-config:set --use-secure 0
      php bin/magento setup:store-config:set --use-secure-admin 0
    fi
  fi
  if [[ -n ${MAGENTO_SECURE_BASEURL+x} ]]; then
    debug "Setting secure baseurl to $MAGENTO_SECURE_BASEURL"
    php bin/magento setup:store-config:set --base-url-secure="$MAGENTO_SECURE_BASEURL"
    php bin/magento setup:store-config:set --use-secure 1
    php bin/magento setup:store-config:set --use-secure-admin 1
  fi
}

# Apply correct permissions for Magento
function magento_fix_permissions() {
  debug "Applying correct permissions to internal folders"
  chmod -R ugoa+rwX var vendor generated pub/static pub/media app/etc
}

function magento_post_deploy {
  debug "Flushing caches"
  php bin/magento cache:flush
  debug "Disabling maintenance mode"
  php bin/magento maintenance:disable
  php bin/magento info:adminuri
}

### Deploy pipe start

# Switch current execution directory to WORKDIR (BASEPATH)
in_basepath
# Installing PHP packages with composer
composer_install

# Flushing Magento configuration in Redis
magento_flush_config
# Custom configuration 
inject_override before_config
# Setting magento database, redis and varnish credentials
magento_config
# Custom configuration
inject_override after_config
# Import Magento configuration into database
magento_pipeline_import_config
# Executing Magento install or migration
magento_database_migration

# Compiling assets, custom comes first
inject_override before_assets
# Static content deploy and DI compile, only in production mode
magento_compile
# Check if admin user exists and create it
create_admin_user

# Set magento deployment
magento_set_mode
# Set varnish configuration
magento_varnish_config
# Set magento baseurl and secure_baseurl
magento_set_baseurl

# Appling correct folder permissions
magento_fix_permissions
# Flushing all caches, removing maintenance mode
magento_post_deploy


# STop the timer and report how much time deployment took to execute
ENDTIME=`date +%s`
let DURATION=${ENDTIME}-${STARTTIME}

HOWLONG=`hms $DURATION`
success "Deployment finished in \033[1;32m${HOWLONG}\033[0m"

if [[ -n ${MAGENTO_MODE+x} ]]; then
  success "Staring php fpm, open in browser $MAGENTO_SECURE_BASEURL\n"
elif [[ -n ${MAGENTO_BASEURL+x} ]]; then
  success "Staring php fpm, open in browser $MAGENTO_BASEURL\n"
else 
  error "Variable MAGENTO_BASEURL and MAGENTO_SECURE_BASEURL are not set"
fi

php-fpm -R