# Magento 2 Template overview

## Template root
Template root is folder, covered by .git and consists of:
`package(-lock).json` - for documentation development and better offline accessibility
 
`docker-compose.yml` - provides a complete description of infrastructure and internal resource sharing describe. **It should not be changed, until you know what you are doing**

 
**Do not change `docker-compose.yml` until you are not adding/removing or changing a default service behavior for ALL
 environments.**
 
`docker-compose.<environment>.yml` - multiple files, that provides overwrites for different environments:
 * local (environment: linux, macos)
 * IMS-INDVP (environment: development, staging) 
 Obsolete files can be removed or more rewrites added for additional environments at any time.
 For more details please refer to [06-running-infrastructure](06-running-infrastructure.md)
 
 ** *docker-compose.staging.yml* and *docker-compose.development.yml* will be merged into **docker-compose.remote
 .yml** in *v1.3*
 
`Dockerfile` - Dockerfile describes build for *App* container. The main container Application is executed with all the necessary tools:
  * PHP: `PHP-FPM` and `PHP-CLI` with `COMPOSER`
  * NodeJS with `NPM` and `N`
  * Ruby with `RBENV` and `GEM`

 
 ### Symlinks
 All docker-compose files are written to provide possibility to override default values defined in the Dockerfile, so do not change `ARG` or `ENV` values in Dockerfile directly.
 .application and .env files are making Docker template extremely flexible, in terms of versioning and modifications. Both files are defined for each environment (*by default: local, dev, staging*) and stored under `deploy/local`, `deploy/development`, `deploy/staging` accordingly.
 
 How to use another ENVVAR set:
 1) Remove currently linked files: `rm -f .application .env`
 2) Link proper ENVVAR set: `ln -s deploy/staging/application .application`, `ln -s deploy/staging/env .env`
 
 `.application` - ENV VARS used by *App* and *MySql* services to keep database configuration up to date on both: MySQL server and MySQL client (App). Provides all the necessary information for magento setup:install execution as well.
 
 `.env` - ENV VARS that specifies internal tool and service versions and setup directory.
 
 ## /deploy folder
 Deploy folder is holding all configurations there are necessary to build/run the containers:
 * ENVVAR sets (`deployment`, `local`, `staging`. **More can be added easily!**),
 * helper shell scripts: `bootstrap.sh`, `entrypoint.sh`, `start.sh`, `wait-for-it.sh`
 * service configs: `shared/conf/nginx`, `shared/conf/php`, `shared/conf/varnish`
  