# Docker on indvp

Running docker on indvp has an ultimate improvement: you can run any software, formed in individual infrastructure, 
while INDVP is still handling SSL certificates and access policies from *Chef*.

In order to succesfully run Docker infrastructure on IMS (indvp) server, you must ensure it is running using *Docker 
setup (current)* template. Choose appropriate on ims.indvp.com while registering the instane or reach any Ops team 
member to update existing instance.

**Important note!** Before switching to new template, ensure you have backed-up all necessary information, for example database or media files.

## Configuration for INDVP
Service configuration are read from env files, that allows to unify templates. For more detials on env files please 
refer to [Structure overview](/02-structure-overview.md)
Env files are stored under `/deploy/development/` or `/deploy/staging` folders.

Additionally to env files, *docker-compose.staging.yml* or *docker-compose.development.yml* must be used during 
deployment with multiple `-f` flags.

** *docker-compose.staging.yml* and *docker-compose.development.yml* will be merged into **docker-compose.remote
.yml** in *v1.3*

### Differencing development and staging environment
As one server is running 2 infrastractures simultaneously we want to be sure these are not colisioning. For this 
purpose we are utilizing Docker-compose PROJECT flag `-p` to set it to currently deployed branch. Just add it to any 
docker-compose command you are running (script or manually), for example: `docker-compose -p staging ps` will output 
status of *up* containers in staging project.
Please refer to [Deployment: Remote tasks](08-deployments.md#Remote-tasks) for more examples.

### INDVP mapping to Docker infrastracture entrypoint
Infrastracture entrypoint is the very first service you suppose traffic goes to. By default it is Varnish (Docker), however you might want to bypass is directly to nginx (Docker).

INDVP configured with *Docker setup (current)* template, provides 2 ports for development and staing environments: 
* *domain*-dev.indvp.com - **:8080**
* *domain*.indvp.com - **:8081**

You can notice, env files for development (deploy/development/env) and staging (deploy/staging/env) environments are 
defining these ports to be mapped to Varnish (Docker) service.

#### Example: Bypassing Varnish, using nginx (Docker) service
* comment out or delete entire Varnish definition in docker-compose.yml(:41-:49)
* edit env file: comment out or delete VARNISH_PORT(:19)
* change NGINX_BIND_PORT respectively
* commit and deploy