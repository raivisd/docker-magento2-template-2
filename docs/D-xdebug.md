# XDebug Setup and Usage

## Setup

php with xdebug is set for local by default, through $PHP_VERSION variable by using `php7.1-debug` as docker image version.

### PhpStorm configuration

#### Browser Plugin

Install browser extensions to supply cookie for enabling debug - <https://confluence.jetbrains.com/display/PhpStorm/Browser+Debugging+Extensions>

#### Debugger settings in IDE

Now to set the PhpStorm, go to:

_Preferences > Languages & Frameworks > PHP > Debug_

In this window chech the following settings:

*   Xdebug section, set port 9111
*   "Can accept external connections" is checked
*   "Ignore external connections through unregistered server configurations" is checked
*   Other settings is up to your preferences of the debugger

Next stop, debugger proxy settings:

_Languages & Frameworks > PHP > Debug > DBGp Proxy_

*   IDE key: PHPSTORM
*   Host: 172.255.255.255
*   Port: 9111

Now we need to set up a server configuration, to map local files to remote application container, head to:

_Languages & Frameworks > PHP > Debug > Servers__

*   Add new configuration by clicking the **+** symbol
*   Host: localhost
*   Port: 3001, which is default nginx port, try to avoid using varnish port and disable varnish in magento configuration during debug sessions
*   Debugger: Xdebug
*   And map your local **src/** directory to remote **/var/www/public**

### Setting IP alias

To allow connections from container to debug console in PhpStorm you need to add following alias to you system, this setting is **not** persisted between restarts, update your env accordingly

**Linux**

`sudo ip address add 172.254.254.254/24 dev docker0`

**macOS**

`sudo ifconfig en0 alias 172.254.254.254 255.255.255.0`

Now the configuration is complete, start the debugger server in right upper corner