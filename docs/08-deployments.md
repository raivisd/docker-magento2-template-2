# Deployments

For successfull deployment setup and debug, you must be aware of registry.kube.indvp.com service and basic principles
 of deployment.
 
Build and deployment configuration will vary, depending on the approach you choose: [Single repository]
(/E-repositories.md#Single-repository) or [Two repositories](/E-repositories.md#Two-repositories).

## Example plans:
[https://bamboo.indvp.com/browse/DOC](https://bamboo.indvp.com/browse/DOC)

Some changes may occur, it is a good idea to refer to the plan.


## Deployment overview
No matter of chosen source repository model, approach, containing a few differences during the built, have very 
similar general flow.

#### Build
* Get Docker configs (service configs and necessary yamls)
* Get source code inside the `/src` folder, please refer to [Deploying two repositorie](#Deploying-two-repositories) 
for details
* Build an image during the build state.
* Push image to registry.kube.indvp.com or compile as compressed package, please refer to [Deploying without 
registry](#Deploying-without-registry)
* Prepare necessary config files to upload to remote server

#### Deploy
* Get prepared artifacts (*Build, step 5*)
* Upload artifacts to remote server
* Execute remote tasks

#### Remote tasks
Please refer to [Docker on indvp: Differencing development and staging environment](07-docker-on-indvp.md#Differencing-development-and-staging-environment) section 
for `-p` explanation.
Docker login, when registry is used:

`echo ${bamboo.DOCKER_DEPLOY_PASSWORD} \
   | sudo docker login -u ${bamboo.DOCKER_DEPLOY_USER} registry.kube.indvp.com --password-stdin
 `

Pull latest images:

`sudo -E docker-compose \
   -p ${bamboo.planRepository.branch} \
   -f docker-compose.yml \
   -f docker-compose.${bamboo.planRepository.branch}.yml \
   pull` 
   
Remove volume holding old code, otherwise it won't be updated:

`sudo -E docker-compose \
   -p ${bamboo.planRepository.branch} \
   -f docker-compose.yml \
   -f docker-compose.${bamboo.planRepository.branch}.yml \
   down
   sudo -E docker volume rm ${bamboo.planRepository.branch}_app-data`
   
Run containers:

`sudo -E docker-compose \
   -p ${bamboo.planRepository.branch} \
   -f docker-compose.yml \
   -f docker-compose.${bamboo.planRepository.branch}.yml \
   up -d \
   --no-build`
   
Remove old images, without the tags (<none> tag, `-f (--filer) dangling=true` does the magic):
`sudo docker rmi $(docker images -q -f dangling=true) | true`


### Deploying two repositories
When building from 2 repositories, at the end you must get the very same setup as with single repository, just some additional artifact configuration is needed.