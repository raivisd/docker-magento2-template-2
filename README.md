# Docker template for Magento 2.x

All docs are in the [docs](docs) folder

#### Development flow 

__master__ branch is a active development branch, once it receives tag, changes are merge into feature branches.
To deploy, use __feature__ branches:
-   `pwa-stable` for PWA enabled development (Magento is latest compatible)
-   `magento23-stable` for Magento 2.3
-   `magento22-stable` for Magento 2.2

## Quick start

1.  Requirements are met, see below
2.  Clone repository and cd to it
3.  Checkout correct branch:
  -  `pwa-stable` for PWA enabled development (Magento is latest compatible)
  -  `magento23-stable` for Magento 2.3
  -  `magento22-stable` for Magento 2.2
4.  `composer.json` must be present in `/src`
5.  Run `make cert` to generate certigicates
6.  Check with `docker-compose ps` that all containers `Running`
7.  Execute `sudo echo >> 127.0.0.1 magento.local` to update hosts file
8.  Once deployment is complete, open <http://magento.local>


## Reporting Issues

Please report any issues by issuing new ticket here: <https://scandiweb.atlassian.net/secure/CreateIssue.jspa?pid=32401>

## Requirements

### Networking

Must be connected to `Scandiweb Network` via Scandiweb WiFi AP or VPN, to pull docker images

To check the connection, run `curl -I -X GET https://registry.kube.indvp.com/harbor/sign-in` and wait for 200

Reference:
[How to connect to Scandiweb VPN](https://scandiweb.atlassian.net/wiki/spaces/NEP/pages/86540295/How+to+connect+to+Scandiweb+VPN)
[Connecting to Scandiweb-Radius Wi-Fi](https://scandiweb.atlassian.net/wiki/spaces/NEP/pages/144244873/Connecting+to+Scandiweb-Radius+Wi-Fi)

### Composer Authentification

For `COMPOSER_AUTH` use your personal Magento 2 key from marketplace. [More info here](docs/A-requirements.md).

### Composer json file

`composer.json` must be present in `/src`
#### Magento 2.3
You can use sample from `/shared`. just issue
`cp deploy/shared/samples/magento2.3/composer.json src/composer.json`

#### Magento 2.2
You can use sample from `/shared`. just issue
`cp deploy/shared/samples/magento2.2/composer.json src/composer.json`


#### ScandiPWA
Take latest stable release from github

`curl -s https://raw.githubusercontent.com/scandipwa/scandipwa-base/master/src/composer.lock -o src/composer.lock`  
`curl -s https://raw.githubusercontent.com/scandipwa/scandipwa-base/master/src/composer.json -o src/composer.json`


### Linux hosts environment variables must be set

_Only for template pre v1.2, not needed for current release_
```
export HOST_UID=$(id -u)
export HOST_GID=$(id -g)
```

```
export COMPOSER_AUTH='{"http-basic":{"repo.magento.com": {"username": "REPLACE_THIS", "password": "REPLACE_THIS"}}}'
```

### macOS hosts environment variables must be set

```
export COMPOSER_AUTH='{"http-basic":{"repo.magento.com": {"username": "REPLACE_THIS", "password": "REPLACE_THIS"}}}'
```

### Troubleshooting

#### See the debug

If something does not work, like you see 404 when opening the site.

1.  Run docker without the `-d` -> `docker-compose -f docker-compose.yml -f docker-compose.local.yml up`
2.  Check logs of running containers by executing: `docker-compose logs -f`

#### Container exited exited with code 1

If you see error like this
```
[Composer\Downloader\TransportException]                                   
app_1      |   The 'https://repo.magento.com/packages.json' URL required authentication.  
app_1      |   You must be using the interactive console to authenticate
```

It means your `COMPOSER_AUTH` is not set or is wrong. 

Please ensure that the keys are correct and that you ran the `export` command in the same terminal window as you run the `docker-compose`

#### Issues with directory permissions

Make sure you can run docker without `sudo`. See [Manage Docker as a non-root user](https://docs.docker.com/install/linux/linux-postinstall/)

#### Can't get into bash in the container

If you see error like this
```
$ docker-compose exec -u user app bash -l
unable to find user user: no matching entries in passwd file
```

There is some issue with your `app` container.

1. Make sure you have latest version of the docker
2. Run `docker-compose -f docker-compose.yml -f docker-compose.local.yml build app`
3. Start containers with `--force-recreate` like this `docker-compose -f docker-compose.yml -f docker-compose.local.yml up --force-recreate`
